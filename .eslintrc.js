module.exports = {
    'parserOptions': { 'ecmaVersion': 2017 },
    'env': { 'es6': true },
    root: true,
    env: {
        node: true,
        jest: true,
    },
    rules: {
        'quotes': ['warn', 'single'],
        'indent': ['error', 4],
        'object-curly-newline': ['warn', { multiline: true }],
        'comma-dangle': ['warn', 'always-multiline'],
        'array-element-newline': ['warn', 'consistent'],
        'object-curly-spacing': ['warn', 'always'],
        'semi': ['warn', 'always'],
    },
    settings: {
        'import/resolver': {
            alias: {
                map: [
                    ['@bms/bigtix-common', '../../libs/common/dist'],
                ],
                extensions: ['.ts', '.js', '.jsx', '.json'],
            },
        },
    },
};
  